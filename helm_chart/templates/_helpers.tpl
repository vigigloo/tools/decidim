{{/* vim: set filetype=mustache: */}}

{{- define "decidim.commonEnv" -}}
- name: RAILS_LOG_TO_STDOUT
  value: "true"
- name: RAILS_LOG_LEVEL
  value: {{ .Values.logLevel | quote }}
- name: RAILS_ENV
  value: {{ .Values.environment | quote }}
- name: SECRET_KEY_BASE
  value: {{ .Values.secret_key_base | quote }}
{{- if and .Values.redis.enabled .Values.redis.url }}
- name: REDIS_URL
  value: {{ .Values.redis.url | quote }}
{{- end }}
{{- with .Values.postgresql }}
- name: DATABASE_PORT
  value: {{ .service.port | default "5432" | quote }}
- name: DATABASE_NAME
  value: {{ .postgresqlDatabase}}
- name: DATABASE_HOST
{{- if .deploy }}
  value: {{ printf "%s-%s" $.Release.Name "postgresql" | trunc 63 | trimSuffix "-" }}
{{- else }}
  value: {{ .postgresqlHost | quote }}
{{- end }}
- name: DATABASE_USERNAME
  value: {{ .postgresqlUsername | quote }}
- name: DATABASE_PASSWORD
{{- if .deploy }}
  valueFrom:
    secretKeyRef:
      name: {{ printf "%s-%s" $.Release.Name "postgresql" | trunc 63 | trimSuffix "-" }}
      key: postgresql-password
{{- else }}
  value: {{ .postgresqlPassword | quote }}
{{- end }}
{{- end }}{{/* end with */}}
{{- with .Values.email }}
{{- if . }}
{{- if .from }}
- name: EMAIL_FROM
  value: {{ .from | quote }}
{{- end }}
{{- with .smtp }}
{{- if . }}
- name: SMTP_DOMAIN
  value: {{ .host | quote }}
- name: SMTP_ADDRESS
  value: {{ .host | quote }}
- name: SMTP_PASSWORD
  value: {{ .password | quote }}
- name: SMTP_PORT
  value: {{ .port | quote }}
- name: SMTP_SECURE
  value: {{ .secure | quote }}
- name: SMTP_USERNAME
  value: {{ .user | quote }}
{{- end }}{{/*end if smtp*/}}
{{- end}}{{/* end with smtp */}}
{{- end }}{{/*end if email*/}}
{{- end }}{{/* end with email */}}
{{- with .Values.s3 }}
{{- if . }}
- name: AWS_ACCESS_KEY_ID
  value: {{ .access_key_id | quote }}
- name: AWS_SECRET_ACCESS_KEY
  value: {{ .secret_access_key | quote }}
- name: AWS_REGION
  value: {{ .region | quote }}
- name: AWS_HOST
  value: {{ .host | quote }}
- name: AWS_BUCKET
  value: {{ .bucket | quote }}
{{- end }}{{/* end if s3 */}}
{{- end }}{{/* end with s3 */}}
{{- if .Values.extraEnv }}
{{- include "common.env.transformDict" .Values.extraEnv -}}
{{- end }}
{{- end }}{{/* end define */}}
