resource "helm_release" "decidim" {
  chart           = "decidim"
  repository      = "https://gitlab.com/api/v4/projects/29542002/packages/helm/stable"
  name            = var.chart_name
  namespace       = var.namespace
  version         = var.chart_version
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history

  values = concat([
    file("${path.module}/decidim.yaml"),
  ], var.values)

  set {
    name  = "secret_key_base"
    value = var.decidim_secret_key_base
  }

  set {
    name  = "environment"
    value = var.decidim_environment
  }

  dynamic "set" {
    for_each = var.image_repository == null ? [] : [var.image_repository]
    content {
      name  = "image.repository"
      value = var.image_repository
    }
  }

  dynamic "set" {
    for_each = var.image_tag == null ? [] : [var.image_tag]
    content {
      name  = "image.tag"
      value = var.image_tag
    }
  }

  dynamic "set" {
    for_each = var.decidim_redis_deploy == null ? [] : [var.decidim_redis_deploy]
    content {
      name  = "redis.deploy"
      value = var.decidim_redis_deploy
    }
  }

  dynamic "set" {
    for_each = var.decidim_redis_enabled == null ? [] : [var.decidim_redis_enabled]
    content {
      name  = "redis.enabled"
      value = var.decidim_redis_enabled
    }
  }

  dynamic "set" {
    for_each = var.decidim_redis_url == null ? [] : [var.decidim_redis_url]
    content {
      name  = "redis.url"
      value = var.decidim_redis_url
    }
  }

  dynamic "set" {
    for_each = var.decidim_postgresql_deploy == null ? [] : [var.decidim_postgresql_deploy]
    content {
      name  = "postgresql.deploy"
      value = var.decidim_postgresql_deploy
    }
  }
  dynamic "set" {
    for_each = var.decidim_postgresql_username == null ? [] : [var.decidim_postgresql_username]
    content {
      name  = "postgresql.postgresqlUsername"
      value = var.decidim_postgresql_username
    }
  }
  dynamic "set" {
    for_each = var.decidim_postgresql_password == null ? [] : [var.decidim_postgresql_password]
    content {
      name  = "postgresql.postgresqlPassword"
      value = var.decidim_postgresql_password
    }
  }
  dynamic "set" {
    for_each = var.decidim_postgresql_database == null ? [] : [var.decidim_postgresql_database]
    content {
      name  = "postgresql.postgresqlDatabase"
      value = var.decidim_postgresql_database
    }
  }
  dynamic "set" {
    for_each = var.decidim_postgresql_host == null ? [] : [var.decidim_postgresql_host]
    content {
      name  = "postgresql.postgresqlHost"
      value = var.decidim_postgresql_host
    }
  }
  dynamic "set" {
    for_each = var.decidim_postgresql_port == null ? [] : [var.decidim_postgresql_port]
    content {
      name  = "postgresql.service.port"
      value = var.decidim_postgresql_port
    }
  }

  dynamic "set" {
    for_each = var.decidim_email_from == null ? [] : [var.decidim_email_from]
    content {
      name  = "email.from"
      value = var.decidim_email_from
    }
  }
  dynamic "set" {
    for_each = var.decidim_email_smtp_host == null ? [] : [var.decidim_email_smtp_host]
    content {
      name  = "email.smtp.host"
      value = var.decidim_email_smtp_host
    }
  }
  dynamic "set" {
    for_each = var.decidim_email_smtp_user == null ? [] : [var.decidim_email_smtp_user]
    content {
      name  = "email.smtp.user"
      value = var.decidim_email_smtp_user
    }
  }
  dynamic "set" {
    for_each = var.decidim_email_smtp_password == null ? [] : [var.decidim_email_smtp_password]
    content {
      name  = "email.smtp.password"
      value = var.decidim_email_smtp_password
    }
  }
  dynamic "set" {
    for_each = var.decidim_email_smtp_port == null ? [] : [var.decidim_email_smtp_port]
    content {
      name  = "email.smtp.port"
      value = var.decidim_email_smtp_port
    }
  }
  dynamic "set" {
    for_each = var.decidim_email_smtp_secure == null ? [] : [var.decidim_email_smtp_secure]
    content {
      name  = "email.smtp.secure"
      value = var.decidim_email_smtp_secure
    }
  }

  dynamic "set" {
    for_each = var.decidim_s3_access_key_id == null ? [] : [var.decidim_s3_access_key_id]
    content {
      name  = "s3.access_key_id"
      value = var.decidim_s3_access_key_id
    }
  }
  dynamic "set" {
    for_each = var.decidim_s3_secret_access_key == null ? [] : [var.decidim_s3_secret_access_key]
    content {
      name  = "s3.secret_access_key"
      value = var.decidim_s3_secret_access_key
    }
  }
  dynamic "set" {
    for_each = var.decidim_s3_region == null ? [] : [var.decidim_s3_region]
    content {
      name  = "s3.region"
      value = var.decidim_s3_region
    }
  }
  dynamic "set" {
    for_each = var.decidim_s3_host == null ? [] : [var.decidim_s3_host]
    content {
      name  = "s3.host"
      value = var.decidim_s3_host
    }
  }
  dynamic "set" {
    for_each = var.decidim_s3_bucket == null ? [] : [var.decidim_s3_bucket]
    content {
      name  = "s3.bucket"
      value = var.decidim_s3_bucket
    }
  }

  dynamic "set" {
    for_each = var.limits_cpu == null ? [] : [var.limits_cpu]
    content {
      name  = "resources.limits.cpu"
      value = var.limits_cpu
    }
  }

  dynamic "set" {
    for_each = var.limits_memory == null ? [] : [var.limits_memory]
    content {
      name  = "resources.limits.memory"
      value = var.limits_memory
    }
  }

  dynamic "set" {
    for_each = var.requests_cpu == null ? [] : [var.requests_cpu]
    content {
      name  = "resources.requests.cpu"
      value = var.requests_cpu
    }
  }

  dynamic "set" {
    for_each = var.requests_memory == null ? [] : [var.requests_memory]
    content {
      name  = "resources.requests.memory"
      value = var.requests_memory
    }
  }

  dynamic "set" {
    for_each = var.ingress_host == null ? [] : [var.ingress_host]
    content {
      name  = "ingress.host"
      value = var.ingress_host
    }
  }

  dynamic "set" {
    for_each = var.decidim_email_from == null ? [] : [var.decidim_email_from]
    content {
      name  = "email.from"
      value = var.decidim_email_from
    }
  }
  dynamic "set" {
    for_each = var.decidim_email_smtp_host == null ? [] : [var.decidim_email_smtp_host]
    content {
      name  = "email.smtp.host"
      value = var.decidim_email_smtp_host
    }
  }
  dynamic "set" {
    for_each = var.decidim_email_smtp_user == null ? [] : [var.decidim_email_smtp_user]
    content {
      name  = "email.smtp.user"
      value = var.decidim_email_smtp_user
    }
  }
  dynamic "set" {
    for_each = var.decidim_email_smtp_password == null ? [] : [var.decidim_email_smtp_password]
    content {
      name  = "email.smtp.password"
      value = var.decidim_email_smtp_password
    }
  }
  dynamic "set" {
    for_each = var.decidim_email_smtp_port == null ? [] : [var.decidim_email_smtp_port]
    content {
      name  = "email.smtp.port"
      value = var.decidim_email_smtp_port
    }
  }
  dynamic "set" {
    for_each = var.decidim_email_smtp_secure == null ? [] : [var.decidim_email_smtp_secure]
    content {
      name  = "email.smtp.secure"
      value = var.decidim_email_smtp_secure
    }
  }
}
