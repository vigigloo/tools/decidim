variable "decidim_secret_key_base" {
  type = string
}

variable "decidim_environment" {
  type    = string
  default = "production"
}

variable "decidim_redis_deploy" {
  type    = bool
  default = false
}

variable "decidim_redis_enabled" {
  type    = bool
  default = true
}

variable "decidim_redis_url" {
  type    = string
  default = null
}

variable "decidim_postgresql_deploy" {
  type    = bool
  default = false
}

variable "decidim_postgresql_username" {
  type    = string
  default = "postgres"
}

variable "decidim_postgresql_password" {
  type    = string
  default = ""
}

variable "decidim_postgresql_database" {
  type    = string
  default = ""
}

variable "decidim_postgresql_host" {
  type    = string
  default = null
}

variable "decidim_postgresql_port" {
  type    = string
  default = 5432
}

variable "decidim_email_from" {
  type    = string
  default = null
}
variable "decidim_email_smtp_host" {
  type    = string
  default = null
}
variable "decidim_email_smtp_user" {
  type    = string
  default = null
}
variable "decidim_email_smtp_password" {
  type    = string
  default = null
}
variable "decidim_email_smtp_port" {
  type    = string
  default = null
}
variable "decidim_email_smtp_secure" {
  type    = string
  default = null
}

variable "decidim_s3_access_key_id" {
  type    = string
  default = null
}
variable "decidim_s3_secret_access_key" {
  type    = string
  default = null
}
variable "decidim_s3_region" {
  type    = string
  default = null
}
variable "decidim_s3_host" {
  type    = string
  default = null
}
variable "decidim_s3_bucket" {
  type    = string
  default = null
}
