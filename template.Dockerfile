ARG RUBY_VERSION=2.7-bullseye
FROM ruby:${RUBY_VERSION} as ruby

ENV RAILS_ENV=production \
  BUNDLE_JOBS=10 \
  BUNDLE_RETRY=3 \
  GEM_HOME=/bundle
ARG BUILD_DECIDIM_VERSION=0.24.3
ENV DECIDIM_VERSION=${BUILD_DECIDIM_VERSION}

ENV DEBIAN_RELEASE=bullseye

ENV NODE_KEYRING=/usr/share/keyrings/nodesource.gpg \
    NODE_VERSION=node_14.x

RUN curl -fsSL https://deb.nodesource.com/gpgkey/nodesource.gpg.key | gpg --dearmor | tee "$NODE_KEYRING" >/dev/null
RUN echo "deb [signed-by=$NODE_KEYRING] https://deb.nodesource.com/$NODE_VERSION $DEBIAN_RELEASE main" | tee /etc/apt/sources.list.d/nodesource.list
RUN echo "deb-src [signed-by=$NODE_KEYRING] https://deb.nodesource.com/$NODE_VERSION $DEBIAN_RELEASE main" | tee -a /etc/apt/sources.list.d/nodesource.list

RUN apt-get update && apt-get install -y libyaml-dev nodejs imagemagick yarn libicu-dev postgresql-client openssl nano bash curl git && apt-get autoremove && apt-get clean all

RUN gem install bundler bootsnap listen decidim:${DECIDIM_VERSION} railties
RUN mkdir -m 770 /decidim && $GEM_HOME/bin/decidim /decidim

WORKDIR /decidim

COPY Gemfile* .

RUN bundle install

COPY . .

RUN $GEM_HOME/bin/rails assets:precompile
RUN $GEM_HOME/bin/rails generate wicked_pdf

RUN chgrp -R 0 /decidim && chmod -R g=rwx /decidim

#FROM gcr.io/distroless/base-debian11

#ENV RAILS_ENV=production
#ENV GEM_HOME=/bundle
#ENV PATH=/usr/local/bundle/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:.
#
#COPY --from=ruby /decidim /decidim
#
#COPY --from=ruby /usr/bin/which /usr/bin/which
#COPY --from=ruby /usr/bin/git /usr/bin/git
#COPY --from=ruby /bin/grep /bin/grep
#COPY --from=ruby /bin/bash /bin/bash
#
#COPY --from=ruby /usr/local/bin/ruby /usr/local/bin/ruby
#COPY --from=ruby /usr/local/include/ /usr/local/include
#COPY --from=ruby /usr/local/lib/ruby/ /usr/local/lib/ruby
#COPY --from=ruby /usr/local/lib/*ruby* /usr/local/lib
#COPY --from=ruby /bundle /bundle
#
#COPY --from=ruby /usr/bin/node /usr/bin/node
#COPY --from=ruby /usr/bin/npm /usr/bin/npm
#
##.so files
#COPY --from=ruby /lib/aarch64-linux-gnu/ /lib/aarch64-linux-gnu
#COPY --from=ruby /usr/lib/aarch64-linux-gnu/ /usr/lib/aarch64-linux-gnu
#
#WORKDIR /decidim

VOLUME /decidim/public/uploads
VOLUME /decidim/config
EXPOSE 3000

ENTRYPOINT ["/bundle/bin/rails"]
CMD ["s"]